//
//  AppDelegate.h
//  dromTest
//
//  Created by Макаров Иван on 18.06.17.
//  Copyright © 2017 Макаров Иван. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

